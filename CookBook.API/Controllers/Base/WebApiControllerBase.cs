﻿namespace CookBook.API.Controllers.Base
{
    using MediatR;
    using Microsoft.AspNetCore.Mvc;
    using System.Threading.Tasks;
    using CookBook.Infrastructure.Helpers;

    [Route("api/[controller]")]
    public class WebApiControllerBase : ControllerBase
    {
        private readonly IMediator _mediator;

        public WebApiControllerBase(IMediator mediator)
        {
            ArgumentChecker.CheckNotNull(new { mediator });

            this._mediator = mediator;
        }

        protected async Task<IActionResult> ProcessAsync(IRequest request)
        {
            if (request == null)
            {
                return this.BadRequest();
            }

            await this._mediator.Send(request);

            return this.NoContent();
        }

        protected async Task<IActionResult> ProcessAsync<TResult>(IRequest<TResult> request)
        {
            if (request == null)
            {
                return this.BadRequest();
            }

            var result = await this._mediator.Send(request);

            if (result == null)
                return this.NotFound();

            return this.Ok(result);
        }

    }
}