﻿namespace CookBook.API.Controllers
{
    using CookBook.API.Controllers.Base;
    using CookBook.API.CQRS.RegionAggregate.Commands;
    using CookBook.API.CQRS.RegionAggregate.Models;
    using CookBook.API.CQRS.RegionAggregate.Queries;
    using CookBook.Infrastructure.Pagination;
    using MediatR;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Threading.Tasks;

    public class RegionController : WebApiControllerBase
    {
        public RegionController(IMediator mediator) : base(mediator)
        {
        }

        [HttpGet("{id}")]
        [ProducesResponseType(200, Type = typeof(RegionResponse))]
        public async Task<IActionResult> Get(Guid id)
        {
            return await this.ProcessAsync(new GetRegionQuery(id));
        }

        [HttpGet("list")]
        [ProducesResponseType(200, Type = typeof(Paged<RegionResponse>))]
        public async Task<IActionResult> List([FromQuery] GetRegionsQuery search)
        {
            return await this.ProcessAsync(search);
        }

        [HttpPost]
        [ProducesResponseType(200)]
        public async Task Post([FromBody] AddRegionCommand command)
        {
            await this.ProcessAsync(command);
        }

        [HttpPut]
        [ProducesResponseType(200)]
        public async Task Put([FromBody] UpdateRegionCommand command)
        {
            await this.ProcessAsync(command);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        public async Task Delete(Guid id)
        {
            await this.ProcessAsync(new DeleteRegionCommand(id));
        }
    }
}
