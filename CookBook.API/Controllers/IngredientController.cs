﻿namespace CookBook.API.Controllers
{
    using CookBook.API.Controllers.Base;
    using CookBook.API.CQRS.DishAggregate.Commands;
    using CookBook.API.CQRS.IngredientAggregate.Commands;
    using CookBook.API.CQRS.IngredientAggregate.Models;
    using CookBook.API.CQRS.IngredientAggregate.Queries;
    using CookBook.Infrastructure.Pagination;
    using MediatR;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Threading.Tasks;

    public class IngredientController : WebApiControllerBase
    {
        public IngredientController(IMediator mediator) : base(mediator)
        {
        }

        [HttpGet("{id}")]
        [ProducesResponseType(200, Type = typeof(IngredientResponse))]
        public async Task<IActionResult> Get(Guid id)
        {
            return await this.ProcessAsync(new GetIngredientQuery(id));
        }

        [HttpGet("list")]
        [ProducesResponseType(200, Type = typeof(Paged<IngredientResponse>))]
        public async Task<IActionResult> List([FromQuery] GetIngredientsQuery search)
        {
            return await this.ProcessAsync(search);
        }

        [HttpPost]
        [ProducesResponseType(200)]
        public async Task Post([FromBody] AddIngredientCommand command)
        {
            await this.ProcessAsync(command);
        }

        [HttpPut]
        [ProducesResponseType(200)]
        public async Task Put([FromBody] UpdateIngredientCommand command)
        {
            await this.ProcessAsync(command);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        public async Task Delete(Guid id)
        {
            await this.ProcessAsync(new DeleteIngredientCommand(id));
        }
    }
}
