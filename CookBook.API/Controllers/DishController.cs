﻿namespace CookBook.API.Controllers
{
    using CookBook.API.Controllers.Base;
    using CookBook.API.CQRS.DishAggregate.Commands;
    using CookBook.API.CQRS.DishAggregate.Models;
    using CookBook.API.CQRS.DishAggregate.Queries;
    using CookBook.Infrastructure.Pagination;
    using MediatR;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Threading.Tasks;

    public class DishController : WebApiControllerBase
    {
        public DishController(IMediator mediator) : base(mediator)
        {
        }

        [HttpGet("{id}")]
        [ProducesResponseType(200, Type = typeof(DishResponse))]
        public async Task<IActionResult> Get(Guid id)
        {
            return await this.ProcessAsync(new GetDishQuery(id));
        }

        [HttpGet("list")]
        [ProducesResponseType(200, Type = typeof(Paged<DishResponse>))]
        public async Task<IActionResult> List([FromQuery] GetDishesQuery search)
        {
            return await this.ProcessAsync(search);
        }

        [HttpPost]
        [ProducesResponseType(200)]
        public async Task Post([FromBody] AddDishCommand command)
        {
            await this.ProcessAsync(command);
        }

        [HttpPut]
        [ProducesResponseType(200)]
        public async Task Put([FromBody] UpdateDishCommand command)
        {
            await this.ProcessAsync(command);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        public async Task Delete(Guid id)
        {
            await this.ProcessAsync(new DeleteDishCommand(id));
        }
    }
}
