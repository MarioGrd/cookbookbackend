﻿namespace CookBook.API.Controllers
{
    using CookBook.API.Controllers.Base;
    using CookBook.API.CQRS.DishAggregate.Queries;
    using CookBook.API.CQRS.IngredientAggregate.Queries;
    using CookBook.API.CQRS.RegionAggregate.Queries;
    using CookBook.API.CQRS.Shared.Models;
    using MediatR;
    using Microsoft.AspNetCore.Mvc;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class AutocompleteController : WebApiControllerBase
    {
        public AutocompleteController(IMediator mediator) : base(mediator)
        {
        }

        [HttpGet("dishes")]
        [ProducesResponseType(200, Type = typeof(List<BasicAutocompleteResponse>))]
        public async Task<IActionResult> AutocompleteDishes([FromQuery] SearchDishQuery search)
        {
            return await this.ProcessAsync(search);
        }

        [HttpGet("regions")]
        [ProducesResponseType(200, Type = typeof(List<BasicAutocompleteResponse>))]
        public async Task<IActionResult> AutocompleteRegions([FromQuery] SearchRegionQuery search)
        {
            return await this.ProcessAsync(search);
        }

        [HttpGet("ingredients")]
        [ProducesResponseType(200, Type = typeof(List<BasicAutocompleteResponse>))]
        public async Task<IActionResult> AutocompleteIngredients([FromQuery] SearchIngredientQuery search)
        {
            return await this.ProcessAsync(search);
        }
    }
}
