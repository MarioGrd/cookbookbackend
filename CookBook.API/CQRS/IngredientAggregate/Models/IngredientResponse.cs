﻿namespace CookBook.API.CQRS.IngredientAggregate.Models
{
    using System;

    public class IngredientResponse
    {
        public Guid DishId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public long Popularity { get; set; }
        public long CommentCount { get; set; }
        public double UserRating { get; set; }
    }
}
