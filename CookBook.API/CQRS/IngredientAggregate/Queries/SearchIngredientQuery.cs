﻿namespace CookBook.API.CQRS.IngredientAggregate.Queries
{
    using CookBook.API.CQRS.Shared.Models;
    using CookBook.API.CQRS.Shared.Queries;
    using CookBook.Domain.IngredientAggregate;
    using CookBook.Infrastructure.Helpers;
    using CookBook.Infrastructure.Repositories.QueryRepositories;
    using FluentValidation;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    public class SearchIngredientQuery : BasicAutocomplete<List<BasicAutocompleteResponse>>
    {
    }

    public class SearchIngredientQueryValidator : AbstractValidator<SearchIngredientQuery>
    {
        public SearchIngredientQueryValidator()
        {
            this.RuleFor(q => q.Query)
                .NotNull();
        }
    }

    public class SearchIngredientQueryHandler : IRequestHandler<SearchIngredientQuery, List<BasicAutocompleteResponse>>
    {
        private readonly IQueryRepository<Ingredient> _ingredientRepository;

        public SearchIngredientQueryHandler(IQueryRepository<Ingredient> ingredientRepository)
        {
            ArgumentChecker.CheckNotNull(new { ingredientRepository });

            this._ingredientRepository = ingredientRepository;
        }

        public async Task<List<BasicAutocompleteResponse>> Handle(SearchIngredientQuery request, CancellationToken token)
        {
            List<BasicAutocompleteResponse> response =
                await this._ingredientRepository.Query
                    .Where(d => d.Name.OriginalName.StartsWith(request.Query))
                    .OrderBy(ob => ob.Name.OriginalName)
                    .Select(d => new BasicAutocompleteResponse()
                    {
                        Id = d.Id,
                        Name = d.Name.OriginalName,
                    })
                    .Skip(0)
                    .Take(request.Take.Value)
                    .ToListAsync();

            return response;
        }
    }
}
