﻿namespace CookBook.API.CQRS.IngredientAggregate.Queries
{
    using CookBook.API.CQRS.IngredientAggregate.Models;
    using CookBook.API.CQRS.Shared.Queries;
    using CookBook.Domain.IngredientAggregate;
    using CookBook.Infrastructure.Helpers;
    using CookBook.Infrastructure.Pagination;
    using CookBook.Infrastructure.Repositories.QueryRepositories;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    public class GetIngredientsQuery : PaginationQuery<Paged<IngredientResponse>>
    {
        public string Query { get; set; }
    }

    public class GetIngredientsQueryHandler : IRequestHandler<GetIngredientsQuery, Paged<IngredientResponse>>
    {
        private readonly IQueryRepository<Ingredient> _ingredientRepository;

        public GetIngredientsQueryHandler(IQueryRepository<Ingredient> ingredientRepository)
        {
            ArgumentChecker.CheckNotNull(new { ingredientRepository });

            this._ingredientRepository = ingredientRepository;
        }

        public async Task<Paged<IngredientResponse>> Handle(GetIngredientsQuery request, CancellationToken token)
        {
            var query =
                this._ingredientRepository.Query
                    .WhereIf(request.Query != null, d => d.Name.OriginalName.StartsWith(request.Query))
                    .Select(d => new IngredientResponse()
                    {
                        DishId = d.Id,
                        Name = d.Name.OriginalName,
                        Description = d.Description,
                        CommentCount = d.Statistic.CommentCount,
                        Popularity = d.Statistic.Popularity,
                        UserRating = d.Statistic.UsersRating
                    });

            var count = await query.CountAsync();
            var data = await query.Skip(request.Skip()).Take(request.PageSize).ToListAsync();

            return new Paged<IngredientResponse>(data, request.PageNumber, request.PageSize, count);
        }
    }
}
