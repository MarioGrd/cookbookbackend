﻿namespace CookBook.API.CQRS.IngredientAggregate.Queries
{
    using CookBook.API.CQRS.IngredientAggregate.Models;
    using CookBook.API.CQRS.Shared.Queries;
    using CookBook.Domain.IngredientAggregate;
    using CookBook.Infrastructure.Helpers;
    using CookBook.Infrastructure.Repositories.QueryRepositories;
    using FluentValidation;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    public class GetIngredientQuery : IdentityQuery<IngredientResponse>
    {
        public GetIngredientQuery(Guid id) : base(id)
        {
        }
    }

    public class GetIngredientQueryValidator : AbstractValidator<GetIngredientQuery>
    {
        public GetIngredientQueryValidator(IQueryRepository<Ingredient> ingredientRepository)
        {
            ArgumentChecker.CheckNotNull(new { ingredientRepository });

            this.RuleFor(q => q.Id)
                .Must(id => id != null)
                .WithMessage($"{nameof(GetIngredientQuery.Id)} cannot be null.")
                .MustAsync(async (id, token) => await ingredientRepository.Query.AnyAsync(s => s.Id == id))
                .WithMessage($"Ingredient not found.");
        }
    }

    public class GetIngredientQueryHandler : IRequestHandler<GetIngredientQuery, IngredientResponse>
    {
        private readonly IQueryRepository<Ingredient> _ingredientRepository;

        public GetIngredientQueryHandler(IQueryRepository<Ingredient> ingredientRepository)
        {
            ArgumentChecker.CheckNotNull(new { ingredientRepository });

            this._ingredientRepository = ingredientRepository;
        }

        public async Task<IngredientResponse> Handle(GetIngredientQuery request, CancellationToken token)
        {
            IngredientResponse response =
                await this._ingredientRepository.Query.Where(d => d.Id == request.Id)
                    .Select(d => new IngredientResponse()
                    {
                        DishId = d.Id,
                        Name = d.Name.OriginalName,
                        Description = d.Description,
                        CommentCount = d.Statistic.CommentCount,
                        Popularity = d.Statistic.Popularity,
                        UserRating = d.Statistic.UsersRating
                    })
                    .SingleOrDefaultAsync();

            return response;
        }
    }
}
