﻿namespace CookBook.API.CQRS.IngredientAggregate.Commands
{
    using CookBook.Domain.IngredientAggregate;
    using CookBook.Domain.RegionAggregate;
    using CookBook.Infrastructure.Helpers;
    using CookBook.Infrastructure.Repositories.QueryRepositories;
    using FluentValidation;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    public class UpdateIngredientCommand : IRequest
    {
        public Guid Id { get; set; }
        public string OriginalName { get; set; }
        public string EnglishName { get; set; }
        public string OtherNames { get; set; }
        public string ShortName { get; set; }

        public int Popularity { get; set; }
        public string Description { get; set; }
        public Guid RegionId { get; set; }
    }

    public class UpdateIngredientCommandValidator : AbstractValidator<UpdateIngredientCommand>
    {
        public UpdateIngredientCommandValidator(IQueryRepository<Region> regionRepository, IQueryRepository<Ingredient> ingredientRepository)
        {
            ArgumentChecker.CheckNotNull(new { regionRepository, ingredientRepository });

            this.RuleFor(p => p.Id)
                .Must(id => id != null)
                .WithMessage($"'{nameof(UpdateIngredientCommand.Id)}' is required.")
                .MustAsync(async (id, token) => await ingredientRepository.Query.AnyAsync(s => s.Id == id))
                .WithMessage($"Ingredient not found.");

            this.RuleFor(p => p.RegionId)
                .Must(id => id != null)
                .WithMessage($"'{nameof(UpdateIngredientCommand.RegionId)}' is required.")
                .MustAsync(async (id, token) => await regionRepository.Query.AnyAsync(s => s.Id == id))
                .WithMessage($"Region not found.");

            this.RuleFor(p => p.OriginalName)
                .NotNull()
                .NotEmpty()
                .WithMessage($"'{nameof(UpdateIngredientCommand.OriginalName)}' is required.");
        }
    }

    public class UpdateIngredientCommandHandler : AsyncRequestHandler<UpdateIngredientCommand>
    {
        private readonly IIngredientDomainRepository _ingredientRepository;

        public UpdateIngredientCommandHandler(IIngredientDomainRepository ingredientRepository)
        {
            ArgumentChecker.CheckNotNull(new { ingredientRepository });

            this._ingredientRepository = ingredientRepository;
        }

        protected override async Task Handle(UpdateIngredientCommand request, CancellationToken cancellationToken)
        {
            Ingredient ingredient = await this._ingredientRepository.GetByIdAsync(request.Id);

            ingredient.Update(
                regionId: request.RegionId,
                originalName: request.OriginalName,
                englishName: request.EnglishName,
                shortName: request.ShortName,
                otherNames: request.OtherNames,
                popularity: request.Popularity,
                description: request.Description);

            this._ingredientRepository.Update(ingredient);
            await this._ingredientRepository.UnitOfWork.SaveChangesAsync();
        }
    }
}
