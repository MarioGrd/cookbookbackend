﻿namespace CookBook.API.CQRS.DishAggregate.Commands
{
    using CookBook.API.CQRS.Shared.Queries;
    using CookBook.Domain.IngredientAggregate;
    using CookBook.Infrastructure.Helpers;
    using CookBook.Infrastructure.Repositories.QueryRepositories;
    using FluentValidation;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    public class DeleteIngredientCommand : IdentityCommand
    {
        public DeleteIngredientCommand(Guid id) : base(id)
        {
        }
    }

    public class DeleteIngredientCommandValidator : AbstractValidator<DeleteIngredientCommand>
    {
        public DeleteIngredientCommandValidator(IQueryRepository<Ingredient> ingredientRepository)
        {
            ArgumentChecker.CheckNotNull(new { ingredientRepository });

            this.RuleFor(p => p.Id)
                .Must(id => id != null)
                .WithMessage($"'{nameof(DeleteIngredientCommand.Id)}' is required.")
                .MustAsync(async (id, token) => await ingredientRepository.Query.AnyAsync(s => s.Id == id))
                .WithMessage($"Ingredient not found.");
        }
    }

    public class DeleteIngredientCommandHandler : AsyncRequestHandler<DeleteIngredientCommand>
    {
        private readonly IIngredientDomainRepository _ingredientRepository;

        public DeleteIngredientCommandHandler(IIngredientDomainRepository ingredientRepository)
        {
            ArgumentChecker.CheckNotNull(new { ingredientRepository });

            this._ingredientRepository = ingredientRepository;
        }

        protected override async Task Handle(DeleteIngredientCommand request, CancellationToken cancellationToken)
        {
            Ingredient ingredient = await this._ingredientRepository.GetByIdAsync(request.Id);

            this._ingredientRepository.Delete(ingredient);

            await this._ingredientRepository.UnitOfWork.SaveChangesAsync();
        }
    }
}
