﻿namespace CookBook.API.CQRS.DishAggregate.Commands
{
    using CookBook.Domain.IngredientAggregate;
    using CookBook.Domain.RegionAggregate;
    using CookBook.Infrastructure.Helpers;
    using CookBook.Infrastructure.Repositories.QueryRepositories;
    using FluentValidation;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    public class AddIngredientCommand : IRequest
    {
        public string OriginalName { get; set; }
        public string EnglishName { get; set; }
        public string OtherNames { get; set; }
        public string ShortName { get; set; }

        public int Popularity { get; set; }
        public string Description { get; set; }
        public Guid RegionId { get; set; }
    }

    public class AddIngredientCommandValidator : AbstractValidator<AddIngredientCommand>
    {
        public AddIngredientCommandValidator(IQueryRepository<Region> regionRepository)
        {
            ArgumentChecker.CheckNotNull(new { regionRepository });

            this.RuleFor(p => p.RegionId)
                .Must(id => id != null)
                .WithMessage($"'{nameof(AddIngredientCommand.RegionId)}' is required.")
                .MustAsync(async (id, token) => await regionRepository.Query.AnyAsync(s => s.Id == id))
                .WithMessage($"Region not found.");

            this.RuleFor(p => p.OriginalName)
                .NotNull()
                .NotEmpty()
                .WithMessage($"'{nameof(AddIngredientCommand.OriginalName)}' is required.");
        }
    }

    public class AddIngredientCommandHandler : AsyncRequestHandler<AddIngredientCommand>
    {
        private readonly IIngredientDomainRepository _ingredientRepository;

        public AddIngredientCommandHandler(IIngredientDomainRepository ingredientRepository)
        {
            ArgumentChecker.CheckNotNull(new { ingredientRepository });

            this._ingredientRepository = ingredientRepository;
        }

        protected override async Task Handle(AddIngredientCommand request, CancellationToken cancellationToken)
        {
            Ingredient ingredient = new Ingredient(
               regionId: request.RegionId,
               originalName: request.OriginalName,
               englishName: request.EnglishName,
               shortName: request.ShortName,
               otherNames: request.OtherNames,
               popularity: request.Popularity,
               description: request.Description);

            this._ingredientRepository.Add(ingredient);
            await this._ingredientRepository.UnitOfWork.SaveChangesAsync();
        }
    }
}
