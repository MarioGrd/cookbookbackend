﻿namespace CookBook.API.CQRS.Shared.Models
{
    using System;

    public class BasicAutocompleteResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
