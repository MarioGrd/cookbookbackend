﻿namespace CookBook.API.CQRS.Shared.Queries
{
    using MediatR;
    using System;

    public class IdentityCommand : IdentityRequest, IRequest
    {
        public IdentityCommand(Guid id) : base(id)
        {
        }
    }
}
