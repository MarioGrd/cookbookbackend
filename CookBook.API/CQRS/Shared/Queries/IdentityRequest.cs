﻿namespace CookBook.API.CQRS.Shared.Queries
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public abstract class IdentityRequest
    {
        public Guid Id { get; private set; }

        public IdentityRequest(Guid id)
        {
            if (id == null)
            {
                throw new ValidationException("Id is required");
            }

            this.Id = id;
        }
    }
}
