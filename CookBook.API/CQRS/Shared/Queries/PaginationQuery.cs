﻿namespace CookBook.API.CQRS.Shared.Queries
{
    using MediatR;

    public class PaginationQuery<T> : IRequest<T>
    {
        private const int DefaultPageSize = 10;

        public int PageNumber { get; set; }
        public int PageSize { get; set; }

        public PaginationQuery()
        {
            this.PageNumber = 1;
            this.PageSize = DefaultPageSize;
        }

        public PaginationQuery(int pageNumber, int pageSize)
        {
            this.PageNumber = pageNumber < 1 ? 1 : pageNumber;
            this.PageSize = pageSize < 1 ? DefaultPageSize : pageSize;
        }

        public int Skip()
        {
            if (this.PageNumber < 1)
                this.PageNumber = 1;

            if (this.PageSize < 1)
                this.PageSize = DefaultPageSize;

            var skip = (this.PageNumber - 1) * this.PageSize;

            return skip;
        }
    }
}
