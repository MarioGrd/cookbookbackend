﻿namespace CookBook.API.CQRS.Shared.Queries
{
    using MediatR;

    public class BasicAutocomplete<T> : IRequest<T>
    {
        private const int DefaultPageSize = 10;

        public string Query { get; set; }
        public int? Take { get; set; }

        public BasicAutocomplete()
        {
            this.Take = this.Take ?? DefaultPageSize;
        }
    }
}
