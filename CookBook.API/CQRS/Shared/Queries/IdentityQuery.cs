﻿namespace CookBook.API.CQRS.Shared.Queries
{
    using MediatR;
    using System;

    public class IdentityQuery<T> : IdentityRequest, IRequest<T>
    {
        public IdentityQuery(Guid id) : base(id)
        {
        }
    }
}
