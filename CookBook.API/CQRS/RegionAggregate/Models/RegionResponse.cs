﻿using System;

namespace CookBook.API.CQRS.RegionAggregate.Models
{
    public class RegionResponse
    {
        public Guid Id { get; set; }
        public string OriginalName { get; set; }
        public string EnglishName { get; set; }
        public string OtherNames { get; set; }
        public string ShortName { get; set; }
        public int RegionLevelId { get; set; }

        public long Popularity { get; set; }
        public long DishCount { get; set; }
        public double IngredientCount { get; set; }
    }
}
