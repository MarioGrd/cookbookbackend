﻿namespace CookBook.API.CQRS.RegionAggregate.Queries
{
    using CookBook.API.CQRS.RegionAggregate.Models;
    using CookBook.API.CQRS.Shared.Queries;
    using CookBook.Domain.RegionAggregate;
    using CookBook.Infrastructure.Helpers;
    using CookBook.Infrastructure.Pagination;
    using CookBook.Infrastructure.Repositories.QueryRepositories;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    public class GetRegionsQuery : PaginationQuery<Paged<RegionResponse>>
    {
        public string Query { get; set; }
    }

    public class GetRegionsQueryHandler : IRequestHandler<GetRegionsQuery, Paged<RegionResponse>>
    {
        private readonly IQueryRepository<Region> _regions;

        public GetRegionsQueryHandler(IQueryRepository<Region> regions)
        {
            ArgumentChecker.CheckNotNull(new { regions });

            this._regions = regions;
        }

        public async Task<Paged<RegionResponse>> Handle(GetRegionsQuery request, CancellationToken token)
        {
            var query =
                this._regions.Query
                    .WhereIf(request.Query != null, d => d.Name.OriginalName.StartsWith(request.Query))
                    .Select(d => new RegionResponse()
                    {
                        Id = d.Id,
                        OriginalName = d.Name.OriginalName,
                        Popularity = d.Statistic.Popularity,
                        DishCount = d.Statistic.DishCount,
                        IngredientCount = d.Statistic.IngredientCount,
                        RegionLevelId = d.RegionLevelId
                    });

            var count = await query.CountAsync();
            var data = await query.Skip(request.Skip()).Take(request.PageSize).ToListAsync();

            return new Paged<RegionResponse>(data, request.PageNumber, request.PageSize, count);
        }
    }
}
