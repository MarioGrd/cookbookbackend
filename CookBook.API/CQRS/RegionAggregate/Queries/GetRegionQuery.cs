﻿namespace CookBook.API.CQRS.RegionAggregate.Queries
{
    using CookBook.API.CQRS.RegionAggregate.Models;
    using CookBook.API.CQRS.Shared.Queries;
    using CookBook.Domain.RegionAggregate;
    using CookBook.Infrastructure.Helpers;
    using CookBook.Infrastructure.Repositories.QueryRepositories;
    using FluentValidation;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    public class GetRegionQuery : IdentityQuery<RegionResponse>
    {
        public GetRegionQuery(Guid id) : base(id)
        {
        }
    }

    public class GetRegionQueryValidator : AbstractValidator<GetRegionQuery>
    {
        public GetRegionQueryValidator(IQueryRepository<Region> dishes)
        {
            ArgumentChecker.CheckNotNull(new { dishes });

            this.RuleFor(q => q.Id)
                .Must(id => id != null)
                .WithMessage($"{nameof(GetRegionQuery.Id)} cannot be null.")
                .MustAsync(async (id, token) => await dishes.Query.AnyAsync(s => s.Id == id))
                .WithMessage($"Region not found.");
        }
    }

    public class GetRegionQueryHandler : IRequestHandler<GetRegionQuery, RegionResponse>
    {
        private readonly IQueryRepository<Region> _regions;

        public GetRegionQueryHandler(IQueryRepository<Region> regions)
        {
            ArgumentChecker.CheckNotNull(new { regions });

            this._regions = regions;
        }

        public async Task<RegionResponse> Handle(GetRegionQuery request, CancellationToken token)
        {
            RegionResponse response =
                await this._regions.Query
                    .Where(d => d.Id == request.Id)
                    .Select(d => new RegionResponse()
                    {
                        Id = d.Id,
                        OriginalName = d.Name.OriginalName,
                        ShortName = d.Name.ShortName,
                        EnglishName = d.Name.EnglishName,
                        RegionLevelId = d.RegionLevelId,
                        OtherNames = d.Name.OtherNames,
                        Popularity = d.Statistic.Popularity,
                        DishCount = d.Statistic.DishCount,
                        IngredientCount = d.Statistic.IngredientCount
                    })
                    .SingleOrDefaultAsync();

            return response;
        }
    }
}
