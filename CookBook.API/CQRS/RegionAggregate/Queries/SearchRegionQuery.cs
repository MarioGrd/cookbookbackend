﻿namespace CookBook.API.CQRS.RegionAggregate.Queries
{
    using CookBook.API.CQRS.Shared.Models;
    using CookBook.API.CQRS.Shared.Queries;
    using CookBook.Domain.RegionAggregate;
    using CookBook.Infrastructure.Helpers;
    using CookBook.Infrastructure.Repositories.QueryRepositories;
    using FluentValidation;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    public class SearchRegionQuery : BasicAutocomplete<List<BasicAutocompleteResponse>>
    {
    }

    public class SearchRegionQueryValidator : AbstractValidator<SearchRegionQuery>
    {
        public SearchRegionQueryValidator()
        {
            this.RuleFor(q => q.Query)
                .NotNull();
        }
    }

    public class SearchRegionQueryHandler : IRequestHandler<SearchRegionQuery, List<BasicAutocompleteResponse>>
    {
        private readonly IQueryRepository<Region> _regions;

        public SearchRegionQueryHandler(IQueryRepository<Region> regions)
        {
            ArgumentChecker.CheckNotNull(new { regions });

            this._regions = regions;
        }

        public async Task<List<BasicAutocompleteResponse>> Handle(SearchRegionQuery request, CancellationToken token)
        {
            List<BasicAutocompleteResponse> response =
                await this._regions.Query
                    .Where(d => d.Name.OriginalName.StartsWith(request.Query))
                    .OrderBy(ob => ob.Name.OriginalName)
                    .Select(d => new BasicAutocompleteResponse()
                    {
                        Id = d.Id,
                        Name = d.Name.OriginalName,
                    })
                    .Skip(0)
                    .Take(request.Take.Value)
                    .ToListAsync();

            return response;
        }
    }
}
