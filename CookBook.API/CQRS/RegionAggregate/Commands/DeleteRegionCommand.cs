﻿namespace CookBook.API.CQRS.RegionAggregate.Commands
{
    using CookBook.API.CQRS.Shared.Queries;
    using CookBook.Domain.RegionAggregate;
    using CookBook.Infrastructure.Helpers;
    using CookBook.Infrastructure.Repositories.QueryRepositories;
    using FluentValidation;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    public class DeleteRegionCommand : IdentityCommand
    {
        public DeleteRegionCommand(Guid id) : base(id)
        {
        }
    }

    public class DeleteRegionCommandValidator : AbstractValidator<DeleteRegionCommand>
    {
        public DeleteRegionCommandValidator(IQueryRepository<Region> regionRepository)
        {
            ArgumentChecker.CheckNotNull(new { regionRepository });

            this.RuleFor(p => p.Id)
                .Must(id => id != null)
                .WithMessage($"'{nameof(DeleteRegionCommand.Id)}' is required.")
                .MustAsync(async (id, token) => await regionRepository.Query.AnyAsync(s => s.Id == id))
                .WithMessage($"Region not found.");
        }
    }

    public class DeleteRegionCommandHandler : AsyncRequestHandler<DeleteRegionCommand>
    {
        private readonly IRegionDomainRepository _regionRepository;

        public DeleteRegionCommandHandler(IRegionDomainRepository regionRepository)
        {
            ArgumentChecker.CheckNotNull(new { regionRepository });

            this._regionRepository = regionRepository;
        }

        protected override async Task Handle(DeleteRegionCommand request, CancellationToken cancellationToken)
        {
            Region region = await this._regionRepository.GetByIdAsync(request.Id);

            this._regionRepository.Delete(region);

            await this._regionRepository.UnitOfWork.SaveChangesAsync();
        }
    }
}
