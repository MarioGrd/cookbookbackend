﻿namespace CookBook.API.CQRS.RegionAggregate.Commands
{
    using BuildingBlocks.Base;
    using CookBook.Domain.RegionAggregate;
    using CookBook.Domain.RegionAggregate.Enumerations;
    using CookBook.Infrastructure.Helpers;
    using FluentValidation;
    using MediatR;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    public class AddRegionCommand : IRequest
    {
        public string OriginalName { get; set; }
        public string EnglishName { get; set; }
        public string OtherNames { get; set; }
        public string ShortName { get; set; }

        public int Popularity { get; set; }
        public int RegionLevelId { get; set; }
    }

    public class AddRegionCommandValidator : AbstractValidator<AddRegionCommand>
    {
        public AddRegionCommandValidator()
        {
            this.RuleFor(p => p.OriginalName)
                .NotNull()
                .NotEmpty()
                .WithMessage($"'{nameof(AddRegionCommand.OriginalName)}' is required.");

            this.RuleFor(p => p.RegionLevelId)
                .Must(id => RegionLevel.List().Any(p => p.Id == id))
                .WithMessage($"'{nameof(AddRegionCommand.RegionLevelId)}' is required.");

            this.RuleFor(p => p.Popularity)
                .GreaterThanOrEqualTo(0)
                .WithMessage($"'{nameof(AddRegionCommand.Popularity)}' must be greater than or equal to 0.");
        }
    }

    public class AddRegionCommandHandler : AsyncRequestHandler<AddRegionCommand>
    {
        private readonly IRegionDomainRepository _regionRepository;

        public AddRegionCommandHandler(IRegionDomainRepository regionRepository)
        {
            ArgumentChecker.CheckNotNull(new { regionRepository });

            this._regionRepository = regionRepository;
        }

        protected override async Task Handle(AddRegionCommand request, CancellationToken cancellationToken)
        {
            Region region = new Region(
                originalName: request.OriginalName,
                englishName: request.EnglishName,
                shortName: request.ShortName,
                otherNames: request.OtherNames,
                regionLevel: Enumeration.FromValue<RegionLevel>(request.RegionLevelId),
                popularity: request.Popularity); 

            this._regionRepository.Add(region);
            await this._regionRepository.UnitOfWork.SaveChangesAsync();
        }
    }
}
