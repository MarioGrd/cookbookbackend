﻿namespace CookBook.API.CQRS.RegionAggregate.Commands
{
    using BuildingBlocks.Base;
    using CookBook.Domain.RegionAggregate;
    using CookBook.Domain.RegionAggregate.Enumerations;
    using CookBook.Infrastructure.Helpers;
    using CookBook.Infrastructure.Repositories.QueryRepositories;
    using FluentValidation;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    public class UpdateRegionCommand : IRequest
    {
        public Guid Id { get; set; }
        public string OriginalName { get; set; }
        public string EnglishName { get; set; }
        public string OtherNames { get; set; }
        public string ShortName { get; set; }
        public int Popularity { get; set; }
        public int RegionLevelId { get; set; }
    }

    public class UpdateRegionCommandValidator : AbstractValidator<UpdateRegionCommand>
    {
        public UpdateRegionCommandValidator(IQueryRepository<Region> regionRepository)
        {
            ArgumentChecker.CheckNotNull(new { regionRepository });

            this.RuleFor(p => p.Id)
                .Must(id => id != null)
                .WithMessage($"'{nameof(UpdateRegionCommand.Id)}' is required.")
                .MustAsync(async (id, token) => await regionRepository.Query.AnyAsync(s => s.Id == id))
                .WithMessage($"Region not found.");

            this.RuleFor(p => p.OriginalName)
                .NotNull()
                .NotEmpty()
                .WithMessage($"'{nameof(UpdateRegionCommand.OriginalName)}' is required.");

            this.RuleFor(p => p.RegionLevelId)
                .Must(id => RegionLevel.List().Any(p => p.Id == id))
                .WithMessage($"'{nameof(AddRegionCommand.RegionLevelId)}' is required.");

            this.RuleFor(p => p.Popularity)
                .GreaterThanOrEqualTo(0)
                .WithMessage($"'{nameof(AddRegionCommand.Popularity)}' must be greater than or equal to 0.");
        }
    }

    public class UpdateRegionCommandHandler : AsyncRequestHandler<UpdateRegionCommand>
    {
        private readonly IRegionDomainRepository _regionRepository;

        public UpdateRegionCommandHandler(IRegionDomainRepository regionRepository)
        {
            ArgumentChecker.CheckNotNull(new { regionRepository });

            this._regionRepository = regionRepository;
        }

        protected override async Task Handle(UpdateRegionCommand request, CancellationToken cancellationToken)
        {
            Region region = await this._regionRepository.GetByIdAsync(request.Id);

            region.Update(
                originalName: request.OriginalName,
                englishName: request.EnglishName,
                shortName: request.ShortName,
                otherNames: request.OtherNames,
                regionLevel: Enumeration.FromValue<RegionLevel>(request.RegionLevelId),
                popularity: request.Popularity);

            this._regionRepository.Update(region);
            await this._regionRepository.UnitOfWork.SaveChangesAsync();
        }
    }
}
