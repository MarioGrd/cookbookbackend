﻿namespace CookBook.API.CQRS.DishAggregate.Models
{
    using System;

    public class DishResponse
    {
        public Guid Id { get; set; }
        public string OriginalName { get; set; }
        public string EnglishName { get; set; }
        public string OtherNames { get; set; }
        public string ShortName { get; set; }
        public string Description { get; set; }

        public long Popularity { get; set; }
        public long CommentCount { get; set; }
        public double UserRating { get; set; }
    }
}
