﻿namespace CookBook.API.CQRS.DishAggregate.Queries
{
    using CookBook.API.CQRS.Shared.Models;
    using CookBook.API.CQRS.Shared.Queries;
    using CookBook.Domain.DishAggregate;
    using CookBook.Infrastructure.Helpers;
    using CookBook.Infrastructure.Repositories.QueryRepositories;
    using FluentValidation;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    public class SearchDishQuery : BasicAutocomplete<List<BasicAutocompleteResponse>>
    {
    }

    public class SearchDishQueryValidator : AbstractValidator<SearchDishQuery>
    {
        public SearchDishQueryValidator()
        {
            this.RuleFor(q => q.Query)
                .NotNull();
        }
    }

    public class SearchDishQueryHandler : IRequestHandler<SearchDishQuery, List<BasicAutocompleteResponse>>
    {
        private readonly IQueryRepository<Dish> _dishes;

        public SearchDishQueryHandler(IQueryRepository<Dish> dishes)
        {
            ArgumentChecker.CheckNotNull(new { dishes });

            this._dishes = dishes;
        }

        public async Task<List<BasicAutocompleteResponse>> Handle(SearchDishQuery request, CancellationToken token)
        {
            List<BasicAutocompleteResponse> response =
                await this._dishes.Query
                    .Where(d => d.Name.OriginalName.StartsWith(request.Query))
                    .OrderBy(ob => ob.Name.OriginalName)
                    .Select(d => new BasicAutocompleteResponse()
                    {
                        Id = d.Id,
                        Name = d.Name.OriginalName,
                    })
                    .Skip(0)
                    .Take(request.Take.Value)
                    .ToListAsync();

            return response;
        }
    }
}
