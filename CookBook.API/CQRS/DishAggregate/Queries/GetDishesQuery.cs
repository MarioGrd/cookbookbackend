﻿namespace CookBook.API.CQRS.DishAggregate.Queries
{
    using CookBook.API.CQRS.DishAggregate.Models;
    using CookBook.API.CQRS.Shared.Queries;
    using CookBook.Domain.DishAggregate;
    using CookBook.Infrastructure.Helpers;
    using CookBook.Infrastructure.Pagination;
    using CookBook.Infrastructure.Repositories.QueryRepositories;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    public class GetDishesQuery : PaginationQuery<Paged<DishResponse>>
    {
        public string Query { get; set; }
    }

    public class GetDishesQueryHandler : IRequestHandler<GetDishesQuery, Paged<DishResponse>>
    {
        private readonly IQueryRepository<Dish> _dishes;

        public GetDishesQueryHandler(IQueryRepository<Dish> dishes)
        {
            ArgumentChecker.CheckNotNull(new { dishes });

            this._dishes = dishes;
        }

        public async Task<Paged<DishResponse>> Handle(GetDishesQuery request, CancellationToken token)
        {
            var query =
                this._dishes.Query
                    .WhereIf(request.Query != null, d => d.Name.OriginalName.StartsWith(request.Query))
                    .Select(d => new DishResponse()
                    {
                        Id = d.Id,
                        OriginalName = d.Name.OriginalName,
                        Description = d.Description,
                        CommentCount = d.Statistic.CommentCount,
                        Popularity = d.Statistic.Popularity,
                        UserRating = d.Statistic.UsersRating
                    });

            var count = await query.CountAsync();
            var data = await query.Skip(request.Skip()).Take(request.PageSize).ToListAsync();

            return new Paged<DishResponse>(data, request.PageNumber, request.PageSize, count);
        }
    }
}
