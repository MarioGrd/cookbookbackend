﻿namespace CookBook.API.CQRS.DishAggregate.Queries
{
    using CookBook.API.CQRS.DishAggregate.Models;
    using CookBook.API.CQRS.Shared.Queries;
    using CookBook.Domain.DishAggregate;
    using CookBook.Infrastructure.Helpers;
    using CookBook.Infrastructure.Repositories.QueryRepositories;
    using FluentValidation;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    public class GetDishQuery : IdentityQuery<DishResponse>
    {
        public GetDishQuery(Guid id) : base(id)
        {
        }
    }

    public class GetDishQueryValidator : AbstractValidator<GetDishQuery>
    {
        public GetDishQueryValidator(IQueryRepository<Dish> dishes)
        {
            ArgumentChecker.CheckNotNull(new { dishes });

            this.RuleFor(q => q.Id)
                .Must(id => id != null)
                .WithMessage($"{nameof(GetDishQuery.Id)} cannot be null.")
                .MustAsync(async (id, token) => await dishes.Query.AnyAsync(s => s.Id == id))
                .WithMessage($"Dish not found.");
        }
    }

    public class GetDishQueryHandler : IRequestHandler<GetDishQuery, DishResponse>
    {
        private readonly IQueryRepository<Dish> _dishes;

        public GetDishQueryHandler(IQueryRepository<Dish> dishes)
        {
            ArgumentChecker.CheckNotNull(new { dishes });

            this._dishes = dishes;
        }

        public async Task<DishResponse> Handle(GetDishQuery request, CancellationToken token)
        {
            DishResponse response =
                await this._dishes.Query.Where(d => d.Id == request.Id)
                    .Select(d => new DishResponse()
                    {
                        Id = d.Id,
                        OriginalName = d.Name.OriginalName,
                        EnglishName = d.Name.EnglishName,
                        ShortName = d.Name.ShortName,
                        OtherNames = d.Name.OtherNames,
                        Description = d.Description,
                        CommentCount = d.Statistic.CommentCount,
                        Popularity = d.Statistic.Popularity,
                        UserRating = d.Statistic.UsersRating
                    })
                    .SingleOrDefaultAsync();

            return response;
        }
    }
}
