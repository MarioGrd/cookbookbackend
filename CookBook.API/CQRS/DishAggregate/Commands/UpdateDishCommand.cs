﻿namespace CookBook.API.CQRS.DishAggregate.Commands
{
    using CookBook.Domain.DishAggregate;
    using CookBook.Domain.RegionAggregate;
    using CookBook.Infrastructure.Helpers;
    using CookBook.Infrastructure.Repositories.QueryRepositories;
    using FluentValidation;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    public class UpdateDishCommand : IRequest
    {
        public Guid Id { get; set; }
        public string OriginalName { get; set; }
        public string EnglishName { get; set; }
        public string OtherNames { get; set; }
        public string ShortName { get; set; }

        public int Popularity { get; set; }
        public string Description { get; set; }
    }

    public class UpdateDishCommandValidator : AbstractValidator<UpdateDishCommand>
    {
        public UpdateDishCommandValidator(IQueryRepository<Region> regionRepository, IQueryRepository<Dish> dishRepository)
        {
            ArgumentChecker.CheckNotNull(new { regionRepository, dishRepository });

            this.RuleFor(p => p.Id)
                .Must(id => id != null)
                .WithMessage($"'{nameof(UpdateDishCommand.Id)}' is required.")
                .MustAsync(async (id, token) => await dishRepository.Query.AnyAsync(s => s.Id == id))
                .WithMessage($"Dish not found.");

            this.RuleFor(p => p.OriginalName)
                .NotNull()
                .NotEmpty()
                .WithMessage($"'{nameof(UpdateDishCommand.OriginalName)}' is required.");
        }
    }

    public class UpdateDishCommandHandler : AsyncRequestHandler<UpdateDishCommand>
    {
        private readonly IDishDomainRepository _dishRepository;

        public UpdateDishCommandHandler(IDishDomainRepository dishRepository)
        {
            ArgumentChecker.CheckNotNull(new { dishRepository });

            this._dishRepository = dishRepository;
        }

        protected override async Task Handle(UpdateDishCommand request, CancellationToken cancellationToken)
        {
            Dish dish = await this._dishRepository.GetByIdAsync(request.Id);

            dish.Update(
                originalName: request.OriginalName, 
                englishName: request.EnglishName, 
                shortName: request.ShortName, 
                otherNames: request.OtherNames,
                popularity: request.Popularity, 
                description: request.Description);

            this._dishRepository.Update(dish);
            await this._dishRepository.UnitOfWork.SaveChangesAsync();
        }
    }
}
