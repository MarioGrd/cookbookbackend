﻿namespace CookBook.API.CQRS.DishAggregate.Commands
{
    using CookBook.Domain.DishAggregate;
    using CookBook.Domain.RegionAggregate;
    using CookBook.Infrastructure.Helpers;
    using CookBook.Infrastructure.Repositories.QueryRepositories;
    using FluentValidation;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    public class AddDishCommand : IRequest
    {
        public string OriginalName { get; set; }
        public string EnglishName { get; set; }
        public string OtherNames { get; set; }
        public string ShortName { get; set; }

        public int Popularity { get; set; }
        public string Description { get; set; }

    }

    public class AddDishCommandValidator : AbstractValidator<AddDishCommand>
    {
        public AddDishCommandValidator(IQueryRepository<Region> regionRepository)
        {
            ArgumentChecker.CheckNotNull(new { regionRepository });

            this.RuleFor(p => p.OriginalName)
                .NotNull()
                .NotEmpty()
                .WithMessage($"'{nameof(AddDishCommand.OriginalName)}' is required.");

            this.RuleFor(p => p.Description)
                .NotNull()
                .NotEmpty()
                .WithMessage($"'{nameof(AddDishCommand.Description)}' is required.");

            this.RuleFor(p => p.Popularity)
                .GreaterThanOrEqualTo(0)
                .WithMessage($"'{nameof(AddDishCommand.Popularity)}' must be greater than or equal to 0.");
        }
    }

    public class AddDishCommandCommandHandler : AsyncRequestHandler<AddDishCommand>
    {
        private readonly IDishDomainRepository _dishRepository;

        public AddDishCommandCommandHandler(IDishDomainRepository dishRepository)
        {
            ArgumentChecker.CheckNotNull(new { dishRepository });

            this._dishRepository = dishRepository;
        }

        protected override async Task Handle(AddDishCommand request, CancellationToken cancellationToken)
        {
            Dish dish = new Dish(
                originalName: request.OriginalName, 
                englishName: request.EnglishName, 
                shortName: request.ShortName, 
                otherNames: request.OtherNames, 
                popularity: request.Popularity, 
                description: request.Description);

            this._dishRepository.Add(dish);
            await this._dishRepository.UnitOfWork.SaveChangesAsync();
        }
    }
}
