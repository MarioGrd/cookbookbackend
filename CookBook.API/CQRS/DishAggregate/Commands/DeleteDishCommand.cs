﻿namespace CookBook.API.CQRS.DishAggregate.Commands
{
    using CookBook.API.CQRS.Shared.Queries;
    using CookBook.Domain.DishAggregate;
    using CookBook.Infrastructure.Helpers;
    using CookBook.Infrastructure.Repositories.QueryRepositories;
    using FluentValidation;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    public class DeleteDishCommand : IdentityCommand
    {
        public DeleteDishCommand(Guid id) : base(id)
        {
        }
    }

    public class DeleteDishCommandValidator : AbstractValidator<DeleteDishCommand>
    {
        public DeleteDishCommandValidator(IQueryRepository<Dish> dishRepository)
        {
            ArgumentChecker.CheckNotNull(new { dishRepository });

            this.RuleFor(p => p.Id)
                .Must(id => id != null)
                .WithMessage($"'{nameof(DeleteDishCommand.Id)}' is required.")
                .MustAsync(async (id, token) => await dishRepository.Query.AnyAsync(s => s.Id == id))
                .WithMessage($"Dish not found.");
        }
    }

    public class DeleteDishCommandHandler : AsyncRequestHandler<DeleteDishCommand>
    {
        private readonly IDishDomainRepository _dishRepository;

        public DeleteDishCommandHandler(IDishDomainRepository dishRepository)
        {
            ArgumentChecker.CheckNotNull(new { dishRepository });

            this._dishRepository = dishRepository;
        }

        protected override async Task Handle(DeleteDishCommand request, CancellationToken cancellationToken)
        {
            Dish dish = await this._dishRepository.GetByIdAsync(request.Id);

            this._dishRepository.Delete(dish);

            await this._dishRepository.UnitOfWork.SaveChangesAsync();
        }
    }
}
