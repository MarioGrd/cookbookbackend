﻿namespace CookBook.API.Infrastructure.Middlewares
{
    using BuildingBlocks.Base;
    using FluentValidation;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc.ModelBinding;
    using Newtonsoft.Json;
    using CookBook.Infrastructure.Helpers;
    using System;
    using System.Net;
    using System.Threading.Tasks;
    using System.Collections.Generic;



    public class ExceptionResponse
    {
        public string Key { get; set; }
        public string Value { get; set; }

        public ExceptionResponse(string key, string value)
        {
            this.Key = key;
            this.Value = value;
        }
    }

    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionMiddleware(RequestDelegate next)
        {
            ArgumentChecker.CheckNotNull(new { next });

            this._next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await this._next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            context.Response.ContentType = "application/json";

            List<ExceptionResponse> exceptions = new List<ExceptionResponse>();

            if (exception is DomainException)
            {
                exceptions.Add(new ExceptionResponse("Domain", exception.Message));
                var result = JsonConvert.SerializeObject(exceptions);

                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return context.Response.WriteAsync(result);

            }
            else if (exception is ValidationException validationException)
            {
                foreach (var error in validationException.Errors)
                {
                    exceptions.Add(new ExceptionResponse(error.PropertyName, error.ErrorMessage));
                }
                var result = JsonConvert.SerializeObject(exceptions);
                context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return context.Response.WriteAsync(result);

            }
            else
            {
                exceptions.Add(new ExceptionResponse("Unexpected", "An unexpected error occured"));
                var result = JsonConvert.SerializeObject(exceptions);
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return context.Response.WriteAsync(result);
            }
        }
    }

    public static class ExceptionMiddlewareExtension
    {
        public static IApplicationBuilder UseExceptionMiddleware(
            this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ExceptionMiddleware>();
        }
    }
}
