﻿namespace CookBook.API.Infrastructure.Pipeline
{
    using FluentValidation;
    using MediatR;
    using CookBook.Infrastructure.Helpers;
    using System.Threading;
    using System.Threading.Tasks;

    public class ValidatorBehavior<TRequest, TResponse>
        : IPipelineBehavior<TRequest, TResponse>
    {
        private readonly IValidatorFactory _validatorFactory;

        public ValidatorBehavior(IValidatorFactory validatorFactory)
        {
            ArgumentChecker.CheckNotNull(new { validatorFactory });

            this._validatorFactory = validatorFactory;
        }

        public async Task<TResponse> Handle(
            TRequest request,
            CancellationToken cancellationToken,
            RequestHandlerDelegate<TResponse> next)
        {
            var validator = this._validatorFactory.GetValidator<TRequest>();

            if (validator != null)
                await validator.ValidateAndThrowAsync(request);

            return await next();
        }
    }
}
