﻿namespace CookBook.API.Infrastructure.Configuration
{
    using Microsoft.Extensions.DependencyInjection;
    using CookBook.Infrastructure.Database;
    using System;
    using System.Diagnostics;
    using CookBook.Infrastructure.Configuration;

    public static class DbSeedConfiguration
    {
        public static void Seed(this IServiceProvider provider, AppSettings settings)
        {
            var context = provider.GetRequiredService<CookBookContext>();

            if (context.Database.EnsureCreated())
            {
                return;
            }

            try
            {
                DbSeedExtension.Seed(context, settings);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }
    }
}
