﻿namespace CookBook.API.Infrastructure.DI
{
    using Autofac;
    using Autofac.Extensions.DependencyInjection;
    using BuildingBlocks.Base;
    using CookBook.API.Infrastructure.Pipeline;
    using CookBook.API.Infrastructure.Validation;
    using CookBook.Domain.DishAggregate;
    using CookBook.Domain.IngredientAggregate;
    using CookBook.Domain.RegionAggregate;
    using CookBook.Infrastructure.Configuration;
    using CookBook.Infrastructure.Database;
    using CookBook.Infrastructure.Repositories.DomainRepositories;
    using CookBook.Infrastructure.Repositories.QueryRepositories;
    using FluentValidation;
    using MediatR;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.DependencyInjection;
    using System;

    public class DependencyInjectionInitializer
    {
        public static IServiceProvider Initialize(IServiceCollection services, AppSettings appSettings)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<IUnitOfWork, CookBookContext>();

            services.AddMediatR(typeof(ValidatorBehavior<,>).Assembly);

            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidatorBehavior<,>));

            var builder = new ContainerBuilder();

            builder.Populate(services);

            builder
                .RegisterType<ValidatorFactory>()
                .As<IValidatorFactory>();

            builder
                .RegisterAssemblyTypes((typeof(ValidatorFactory).Assembly))
                .AsClosedTypesOf(typeof(IValidator<>));

            builder
                .RegisterGeneric(typeof(QueryRepository<>))
                .As(typeof(IQueryRepository<>))
                .InstancePerLifetimeScope();

            RegisterDomainRepositories(builder);

            return builder.Build().Resolve<IServiceProvider>();
        }

        /// <summary>
        /// Registers strongly typed domain repositories.
        /// </summary>
        /// <param name="builder">Autofac container builder</param>
        public static void RegisterDomainRepositories(ContainerBuilder builder)
        {
            builder
                .RegisterType<DishDomainRepository>()
                .As(typeof(IDishDomainRepository))
                .InstancePerLifetimeScope();

            builder
                .RegisterType<IngredientDomainRepository>()
                .As(typeof(IIngredientDomainRepository))
                .InstancePerLifetimeScope();

            builder
                .RegisterType<RegionDomainRepository>()
                .As(typeof(IRegionDomainRepository))
                .InstancePerLifetimeScope();
        }
    }
}
