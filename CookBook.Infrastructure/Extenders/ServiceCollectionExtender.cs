﻿namespace CookBook.Infrastructure.Extenders
{
    using CookBook.Infrastructure.Configuration;
    using CookBook.Infrastructure.Database;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;
    using System.Reflection;

    public static class ServiceCollectionExtender
    {
        public static IServiceCollection AddCookBookDbContext(this IServiceCollection services, ConnectionStrings strings)
        {
            services.AddDbContext<CookBookContext>(options =>
            {
                options.UseSqlServer(
                    strings.Main,
                    config => config.MigrationsAssembly(Assembly.GetExecutingAssembly().FullName));
            });

            return services;
        }
    }
}
