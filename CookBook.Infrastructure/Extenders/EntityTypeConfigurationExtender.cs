﻿namespace CookBook.Infrastructure.Extenders
{
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    public static class EntityTypeConfigurationExtender
    {
        private static readonly Dictionary<Assembly, IEnumerable<Type>> AssemblyTypes
            = new Dictionary<Assembly, IEnumerable<Type>>();

        /// <summary>
        /// Applies all of the <see cref="IEntityTypeConfiguration{TEntity}"/> found in provided lookup assemblies.
        /// </summary>
        /// <remarks>
        /// If no assemblies are provided, action defaults to the assembly of this extender.
        /// </remarks>
        /// <param name="modelBuilder">
        /// Provides a simple API surface for configuring a <see cref="Microsoft.EntityFrameworkCore.Metadata.IMutableModel"/>
        /// that defines the shape of your entities, the relationships between them, and how they map to the database.
        /// </param>
        /// </param>
        /// <param name="lookupAssemblies">
        /// Assemblies to loop through while looking for <see cref="IEntityTypeConfiguration{TEntity}"/> for your models.
        /// </param>
        /// <returns>The current <see cref="ModelBuilder"/> thus enabling the method chaining.</returns>
        public static ModelBuilder ApplyEntityTypeConfigurations(this ModelBuilder modelBuilder, params Assembly[] lookupAssemblies)
        {
            if (lookupAssemblies == null || lookupAssemblies.Length == 0)
            {
                lookupAssemblies = new Assembly[] { typeof(EntityTypeConfigurationExtender).Assembly };
            }

            foreach (var assembly in lookupAssemblies)
            {
                if (!AssemblyTypes.TryGetValue(assembly, out IEnumerable<Type> configurationTypes))
                {
                    configurationTypes = assembly
                        .GetExportedTypes()
                        .Where(p => p.GetTypeInfo().IsClass)
                        .Where(p => !p.GetTypeInfo().IsAbstract)
                        .Where(p => p.GetInterfaces()
                                     .Where(i => i.GetTypeInfo().IsGenericType)
                                     .Where(i => i.GetGenericTypeDefinition() == typeof(IEntityTypeConfiguration<>))
                                     .Any());

                    EntityTypeConfigurationExtender.AssemblyTypes[assembly] = configurationTypes;
                }

                var configurations = configurationTypes.Select(p => Activator.CreateInstance(p));

                foreach (dynamic configuration in configurations)
                {
                    modelBuilder.ApplyConfiguration(configuration);
                }
            }

            return modelBuilder;
        }
    }
}
