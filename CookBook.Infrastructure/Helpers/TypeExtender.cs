﻿namespace CookBook.Infrastructure.Helpers
{
    using System;
    using System.Linq;
    using System.Reflection;

    public static class TypeExtender
    {
        public static bool HasAttribute<TAttribute>(this Type type)
            where TAttribute : Attribute
        {
            return type
                .GetTypeInfo()
                .GetCustomAttributes<TAttribute>(false)
                .Any();
        }
    }
}
