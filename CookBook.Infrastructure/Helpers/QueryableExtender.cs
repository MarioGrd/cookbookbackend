﻿namespace CookBook.Infrastructure.Helpers
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;

    public static class QueryableExtender
    {
        public static IQueryable<TEntity> WhereIf<TEntity>(
            this IQueryable<TEntity> query,
            bool condition,
            Expression<Func<TEntity, bool>> predicate) where TEntity : class
        {
            return condition ? query.Where(predicate) : query;
        }
    }
}
