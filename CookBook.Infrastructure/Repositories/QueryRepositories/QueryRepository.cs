﻿namespace CookBook.Infrastructure.Repositories.QueryRepositories
{
    using BuildingBlocks.Base;
    using Microsoft.EntityFrameworkCore;
    using CookBook.Infrastructure.Database;
    using CookBook.Infrastructure.Helpers;
    using System.Linq;

    public interface IQueryRepository<TEntity> where TEntity : class
    {
        /// <summary>
        /// Returns <see cref="IQueryable{TEntity}"/> with no tracking.
        /// </summary>
        IQueryable<TEntity> Query { get; }
    }

    public class QueryRepository<TEntity> : IQueryRepository<TEntity> where TEntity : Entity
    {
        private readonly DbSet<TEntity> _dbSet;

        /// <summary>
        /// Returns <see cref="IQueryable{TEntity}"/>
        /// </summary>
        public IQueryable<TEntity> Query
        {
            get
            {
                return this._dbSet.AsNoTracking();
            }
        }

        public QueryRepository(CookBookContext dbContext)
        {
            ArgumentChecker.CheckNotNull(new { dbContext });

            this._dbSet = dbContext.Set<TEntity>();
        }
    }
}
