﻿namespace CookBook.Infrastructure.Repositories.DomainRepositories
{
    using BuildingBlocks.Base;
    using CookBook.Domain.DishAggregate;
    using CookBook.Infrastructure.Database;
    using CookBook.Infrastructure.Helpers;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Threading.Tasks;

    public class DishDomainRepository : IDishDomainRepository
    {
        private readonly CookBookContext _context;
        private readonly DbSet<Dish> _dishes;

        public IUnitOfWork UnitOfWork => this._context;

        public DishDomainRepository(CookBookContext context)
        {
            ArgumentChecker.CheckNotNull(new { context });

            this._context = context;
            this._dishes = context.Set<Dish>();
        }

        public void Add(Dish aggregateRoot)
        {
            this._dishes.Add(aggregateRoot);
        }

        public void Delete(Dish aggregateRoot)
        {
            this._dishes.Remove(aggregateRoot);
        }

        public async Task<Dish> GetByIdAsync(Guid id)
        {
            Dish dish = await this._dishes.SingleOrDefaultAsync(d => d.Id == id);

            if (dish == null)
            {
                throw new DomainRepositoryException("Dish not found.", nameof(DishDomainRepository));
            }

            return dish;
        }

        public void Update(Dish aggregateRoot)
        {
            this._dishes.Update(aggregateRoot);
        }
    }
}
