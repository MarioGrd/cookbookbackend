﻿namespace CookBook.Infrastructure.Repositories.DomainRepositories
{
    using BuildingBlocks.Base;
    using CookBook.Domain.IngredientAggregate;
    using CookBook.Infrastructure.Database;
    using CookBook.Infrastructure.Helpers;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Threading.Tasks;

    public class IngredientDomainRepository : IIngredientDomainRepository
    {
        private readonly CookBookContext _context;
        private readonly DbSet<Ingredient> _ingredients;

        public IUnitOfWork UnitOfWork => this._context;

        public IngredientDomainRepository(CookBookContext context)
        {
            ArgumentChecker.CheckNotNull(new { context });

            this._context = context;
            this._ingredients = context.Set<Ingredient>();
        }

        public void Add(Ingredient aggregateRoot)
        {
            this._ingredients.Add(aggregateRoot);
        }

        public void Delete(Ingredient aggregateRoot)
        {
            this._ingredients.Remove(aggregateRoot);
        }

        public async Task<Ingredient> GetByIdAsync(Guid id)
        {
            Ingredient dish = await this._ingredients.SingleOrDefaultAsync(d => d.Id == id);

            if (dish == null)
            {
                throw new DomainRepositoryException("Ingredient not found.", nameof(IngredientDomainRepository));
            }

            return dish;
        }

        public void Update(Ingredient aggregateRoot)
        {
            this._ingredients.Update(aggregateRoot);
        }
    }
}
