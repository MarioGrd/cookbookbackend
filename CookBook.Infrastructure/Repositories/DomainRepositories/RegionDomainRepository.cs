﻿namespace CookBook.Infrastructure.Repositories.DomainRepositories
{
    using BuildingBlocks.Base;
    using CookBook.Domain.RegionAggregate;
    using CookBook.Infrastructure.Database;
    using CookBook.Infrastructure.Helpers;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Threading.Tasks;

    public class RegionDomainRepository : IRegionDomainRepository
    {
        private readonly CookBookContext _context;
        private readonly DbSet<Region> _regions;

        public IUnitOfWork UnitOfWork => this._context;

        public RegionDomainRepository(CookBookContext context)
        {
            ArgumentChecker.CheckNotNull(new { context });

            this._context = context;
            this._regions = this._context.Set<Region>();
        }


        public void Add(Region aggregateRoot)
        {
            this._regions.Add(aggregateRoot);
        }

        public void Delete(Region aggregateRoot)
        {
            this._regions.Remove(aggregateRoot);
        }

        public async Task<Region> GetByIdAsync(Guid id)
        {
            Region region = await this._regions.SingleOrDefaultAsync(r => r.Id == id);

            if (region == null)
            {
                throw new DomainRepositoryException("Region not found.", nameof(RegionDomainRepository));
            }

            return region;
        }

        public void Update(Region aggregateRoot)
        {
            this._regions.Update(aggregateRoot);
        }
    }
}
