﻿namespace CookBook.Infrastructure.Filters
{
    public class PaginationFilter
    {
        private const int DefaultPageSize = 10;

        public int PageNumber { get; set; }
        public int PageSize { get; set; }

        public PaginationFilter()
        {
            this.PageNumber = 1;
            this.PageSize = DefaultPageSize;
        }

        public PaginationFilter(int pageNumber, int pageSize)
        {
            this.PageNumber = pageNumber < 1 ? 1 : pageNumber;
            this.PageSize = pageSize < 1 ? DefaultPageSize : pageSize;
        }

        public int Skip
        {
            get
            {
                if (this.PageNumber < 1)
                    this.PageNumber = 1;

                if (this.PageSize < 1)
                    this.PageSize = DefaultPageSize;

                var skip = (this.PageNumber - 1) * this.PageSize;

                return skip;
            }
        }
    }
}
