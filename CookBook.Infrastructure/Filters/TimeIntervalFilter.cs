﻿namespace CookBook.Infrastructure.Filters
{
    using System;

    public class TimeIntervalFilter
    {
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
    }
}
