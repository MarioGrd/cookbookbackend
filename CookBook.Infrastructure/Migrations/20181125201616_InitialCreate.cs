﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CookBook.Infrastructure.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Dishes",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Name_OriginalName = table.Column<string>(nullable: false),
                    Name_EnglishName = table.Column<string>(nullable: true),
                    Name_ShortName = table.Column<string>(nullable: true),
                    Name_OtherNames = table.Column<string>(nullable: true),
                    Statistic_CommentCount = table.Column<long>(nullable: false),
                    Statistic_Popularity = table.Column<long>(nullable: false),
                    Statistic_UsersRating = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Dishes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Regions",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name_OriginalName = table.Column<string>(nullable: false),
                    Name_EnglishName = table.Column<string>(nullable: true),
                    Name_ShortName = table.Column<string>(nullable: true),
                    Name_OtherNames = table.Column<string>(nullable: true),
                    Statistic_Popularity = table.Column<int>(nullable: false),
                    Statistic_DishCount = table.Column<int>(nullable: false),
                    Statistic_IngredientCount = table.Column<int>(nullable: false),
                    RegionLevelId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Regions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Ingredients",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Name_OriginalName = table.Column<string>(nullable: false),
                    Name_EnglishName = table.Column<string>(nullable: true),
                    Name_ShortName = table.Column<string>(nullable: true),
                    Name_OtherNames = table.Column<string>(nullable: true),
                    Statistic_CommentCount = table.Column<long>(nullable: false),
                    Statistic_Popularity = table.Column<long>(nullable: false),
                    Statistic_UsersRating = table.Column<double>(nullable: false),
                    RegionId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ingredients", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Ingredients_Regions_RegionId",
                        column: x => x.RegionId,
                        principalTable: "Regions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Ingredients_RegionId",
                table: "Ingredients",
                column: "RegionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Dishes");

            migrationBuilder.DropTable(
                name: "Ingredients");

            migrationBuilder.DropTable(
                name: "Regions");
        }
    }
}
