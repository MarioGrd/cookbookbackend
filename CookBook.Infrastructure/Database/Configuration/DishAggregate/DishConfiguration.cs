﻿namespace CookBook.Infrastructure.Database.Configuration.DishAggregate
{
    using CookBook.Domain.DishAggregate;
    using CookBook.Domain.RegionAggregate;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class DishConfiguration : IEntityTypeConfiguration<Dish>
    {
        public void Configure(EntityTypeBuilder<Dish> builder)
        {
            builder.ToTable("Dishes");

            builder.HasKey(p => p.Id);

            builder.OwnsOne(p => p.Name, ot => ot.Property(p => p.OriginalName).IsRequired());
            builder.OwnsOne(p => p.Statistic);

            builder.Ignore(p => p.DomainEvents);
        }
    }
}
