﻿namespace CookBook.Infrastructure.Database.Configuration.RegionAggregate
{
    using CookBook.Domain.RegionAggregate;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class RegionConfiguration : IEntityTypeConfiguration<Region>
    {
        public void Configure(EntityTypeBuilder<Region> builder)
        {
            builder.ToTable("Regions");

            builder.HasKey(p => p.Id);

            builder.OwnsOne(p => p.Name, ot => ot.Property(p => p.OriginalName).IsRequired());
            builder.OwnsOne(p => p.Statistic);

            builder.Ignore(p => p.DomainEvents);
        }
    }
}
