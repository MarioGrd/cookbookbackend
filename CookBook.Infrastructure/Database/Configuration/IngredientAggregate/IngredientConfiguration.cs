﻿namespace CookBook.Infrastructure.Database.Configuration.IngredientAggregate
{
    using CookBook.Domain.IngredientAggregate;
    using CookBook.Domain.RegionAggregate;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class IngredientConfiguration: IEntityTypeConfiguration<Ingredient>
    {
        public void Configure(EntityTypeBuilder<Ingredient> builder)
        {
            builder.ToTable("Ingredients");

            builder.HasKey(p => p.Id);

            builder.OwnsOne(p => p.Name, ot => ot.Property(p => p.OriginalName).IsRequired());
            builder.OwnsOne(p => p.Statistic);

            builder
                .HasOne(typeof(Region))
                .WithMany()
                .HasForeignKey(nameof(Ingredient.RegionId))
                .IsRequired();

            builder.Ignore(p => p.DomainEvents);
        }
    }
}
