﻿namespace CookBook.Infrastructure.Database
{
    using CookBook.Domain.DishAggregate;
    using CookBook.Domain.IngredientAggregate;
    using CookBook.Domain.RegionAggregate;
    using CookBook.Domain.RegionAggregate.Enumerations;
    using CookBook.Infrastructure.Configuration;
    using System.Collections.Generic;
    using System.Linq;

    public static class DbSeedExtension
    {
        public static void Seed(CookBookContext context, AppSettings settings)
        {
            if (context.Set<Dish>().Any())
            {
                return;
            }

            List<Region> regions = new List<Region>();

            foreach (RegionData region in settings.Regions)
            {
                regions.Add(
                    new Region(
                        region.OriginalName ?? "Original name", 
                        region.EnglishName, 
                        region.ShortName, 
                        region.OtherNames, 
                        RegionLevel.COUNTRY, 
                        region.Popularity));
            }

            context.Set<Region>().AddRange(regions);
            context.SaveChanges();

            List<Dish> dishes = new List<Dish>();

            foreach(DishData dish in settings.Dishes)
            {
                dishes.Add(
                    new Dish(
                        dish.OriginalName ?? "Original name",
                        dish.EnglishName,
                        dish.ShortName,
                        dish.OtherNames,
                        dish.Popularity,
                        dish.Description
                        ));
            }

            context.Set<Dish>().AddRange(dishes);
            context.SaveChanges();

            List<Ingredient> ingredients = new List<Ingredient>();

            foreach (IngredientData ingredient in settings.Ingredients)
            {
                ingredients.Add(
                    new Ingredient(
                        regions[0].Id,
                        ingredient.OriginalName ?? "Original name",
                        ingredient.EnglishName,
                        ingredient.ShortName,
                        ingredient.OtherNames,
                        ingredient.Description,
                        ingredient.Popularity
                        ));
            }

            context.Set<Ingredient>().AddRange(ingredients);
            context.SaveChanges();
        }
    }
}
