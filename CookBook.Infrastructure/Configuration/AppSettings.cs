﻿namespace CookBook.Infrastructure.Configuration
{
    public class AppSettings
    {
        public ConnectionStrings ConnectionStrings { get; set; }
        public TokenSettings TokenSettings { get; set; }
        public PasswordSettings PasswordSettings { get; set; }

        public RegionData[] Regions { get; set; }
        public DishData[] Dishes { get; set; }
        public IngredientData[] Ingredients { get; set; }

    }

    public class ConnectionStrings
    {
        public string Main { get; set; }
    }

    public class TokenSettings
    {
        public string Key { get; set; }
        public string Issuer { get; set; }
        public string Auidence { get; set; }
        public string ExpirationMinutes { get; set; }
    }

    public class PasswordSettings
    {
        public string IterationCount { get; set; }
    }

    public class RegionData
    {
        public string OriginalName { get; set; }
        public string EnglishName { get; set; }
        public string ShortName { get; set; }
        public string OtherNames { get; set; }
        public int RegionLevel { get; set; }
        public int Popularity { get; set; }
    }

    public class DishData
    {
        public string OriginalName { get; set; }
        public string EnglishName { get; set; }
        public string ShortName { get; set; }
        public string OtherNames { get; set; }
        public string Description { get; set; }
        public int Popularity { get; set; }
    }

    public class IngredientData
    {
        public string OriginalName { get; set; }
        public string EnglishName { get; set; }
        public string ShortName { get; set; }
        public string OtherNames { get; set; }
        public string Description { get; set; }
        public int Popularity { get; set; }
    }
}
