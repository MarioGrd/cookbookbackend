﻿namespace CookBook.Infrastructure.Pagination
{
    using System.Collections.Generic;

    public class Paged<TEntity> where TEntity : class
    {
        public List<TEntity> Data { get; private set; }
        public int PageNumber { get; private set; }
        public int PageSize { get; private set; }
        public int Count { get; private set; }

        public Paged(List<TEntity> data, int pageNumber, int pageSize, int count)
        {
            this.Data = data;
            this.PageNumber = pageNumber;
            this.PageSize = pageSize;
            this.Count = count;
        }
    }
}
