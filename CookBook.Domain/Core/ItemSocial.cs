﻿namespace CookBook.Domain.Core
{
    using BuildingBlocks.Base;
    using CookBook.Domain.DishAggregate.Exceptions;
    using System.Collections.Generic;

    public class ItemSocial : ValueObject
    {
        public string Title { get; private set; }
        public string Description { get; private set; }
        public string Image { get; private set; }

        public ItemSocial() { }

        public ItemSocial(string title, string description, string image)
        {
            if (string.IsNullOrWhiteSpace(title))
            {
                throw new DishDomainException("Title is required.");
            }
            if (string.IsNullOrWhiteSpace(description))
            {
                throw new DishDomainException("Description is required.");
            }

            if (string.IsNullOrWhiteSpace(image))
            {
                throw new DishDomainException("Image is required.");
            }

            this.Title = title;
            this.Image = image;
            this.Description = description;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return this.Title;
            yield return this.Description;
            yield return this.Image;
        }
    }
}
