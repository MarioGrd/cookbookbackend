﻿namespace CookBook.Domain.Core
{
    using BuildingBlocks.Base;
    using System.Collections.Generic;

    public class ItemStatistic : ValueObject
    {
        public long CommentCount { get; private set; }
        public long Popularity { get; private set; }
        public double UsersRating { get; private set; }

        public ItemStatistic(long commentCount, long popularity, double usersRating)
        {
            this.CommentCount = commentCount > 0 ? commentCount : 0;
            this.Popularity = popularity > 0 ? popularity : 0;
            this.UsersRating = usersRating > 0 ? usersRating : 0;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return this.CommentCount;
            yield return this.Popularity;
            yield return this.UsersRating;
        }
    }
}
