﻿namespace CookBook.Domain.Core
{
    using BuildingBlocks.Base;
    using CookBook.Domain.DishAggregate.Exceptions;
    using System.Collections.Generic;

    public class ItemName : ValueObject
    {
        public string OriginalName { get; private set; }
        public string EnglishName { get; private set; }
        public string ShortName { get; private set; }
        public string OtherNames { get; private set; }

        public ItemName(string originalName, string englishName = null, string shortName = null, string otherNames = null)
        {
            if (string.IsNullOrEmpty(originalName))
            {
                throw new DishDomainException("Original name is required.");
            }

            this.OriginalName = originalName;
            this.EnglishName = englishName;
            this.ShortName = shortName;
            this.OtherNames = otherNames;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return this.OriginalName;
            yield return this.EnglishName;
            yield return this.ShortName;
            yield return this.OtherNames;
        }
    }
}
