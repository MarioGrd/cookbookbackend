﻿namespace CookBook.Domain.RegionAggregate.Enumerations
{
    using BuildingBlocks.Base;
    using System.Collections.Generic;

    public class RegionLevel : Enumeration
    {
        public static RegionLevel CITY = new RegionLevel(1, nameof(CITY).ToLowerInvariant());
        public static RegionLevel REGION = new RegionLevel(2, nameof(REGION).ToLowerInvariant());
        public static RegionLevel COUNTRY = new RegionLevel(3, nameof(COUNTRY).ToLowerInvariant());
        public static RegionLevel CONTINENT = new RegionLevel(4, nameof(CONTINENT).ToLowerInvariant());

        public RegionLevel() { }

        protected RegionLevel(int id, string name) : base(id, name) { }

        public static IEnumerable<RegionLevel> List() => new[] { CITY, REGION, COUNTRY, CONTINENT };
    }
}
