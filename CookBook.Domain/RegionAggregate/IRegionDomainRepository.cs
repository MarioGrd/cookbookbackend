﻿namespace CookBook.Domain.RegionAggregate
{
    using BuildingBlocks.Base;

    public interface IRegionDomainRepository : IDomainRepository<Region>
    {
    }
}
