﻿using BuildingBlocks.Base;
using System.Collections.Generic;

namespace CookBook.Domain.RegionAggregate
{
    public class RegionStatistic : ValueObject
    {
        public int Popularity { get; private set; }
        public int DishCount { get; private set; }
        public int IngredientCount { get; private set; }

        public RegionStatistic(int popularity, int dishCount, int ingredientCount)
        {
            this.Popularity = popularity > 0 ? popularity : 0;
            this.DishCount = dishCount > 0 ? dishCount : 0;
            this.IngredientCount = ingredientCount > 0 ? ingredientCount : 0;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return this.DishCount;
            yield return this.IngredientCount;
        }
    }
}
