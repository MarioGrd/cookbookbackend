﻿namespace CookBook.Domain.RegionAggregate.Exceptions
{
    using BuildingBlocks.Base;

    public class RegionDomainException : DomainException
    {
        public RegionDomainException(string message) : base(message)
        {
        }
    }
}
