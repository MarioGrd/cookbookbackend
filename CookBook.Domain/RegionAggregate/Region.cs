﻿namespace CookBook.Domain.RegionAggregate
{
    using BuildingBlocks.Base;
    using CookBook.Domain.Core;
    using CookBook.Domain.RegionAggregate.Enumerations;
    using System;

    public class Region : Entity, IAggregateRoot
    {
        public ItemName Name { get; private set; }
        public RegionStatistic Statistic { get; private set; }

        public int RegionLevelId { get; private set; }

        private Region() { }

        public Region(
            string originalName,
            string englishName,
            string shortName, 
            string otherNames,
            RegionLevel regionLevel, 
            int popularity)
        {
            this.Id = Guid.NewGuid();
            this.RegionLevelId = regionLevel.Id;
            this.AddNames(originalName, englishName, shortName, otherNames);
            this.AddStatistic(popularity, 0, 0);
        }

        public void Update(
            string originalName,
            string englishName,
            string shortName,
            string otherNames,
            RegionLevel regionLevel,
            int popularity)
        {
            this.RegionLevelId = regionLevel.Id;
            this.AddNames(originalName, englishName, shortName, otherNames);
            this.AddStatistic(popularity, 0, 0);
        }

        private void AddNames(string originalName, string englishName = null, string shortName = null, string otherNames = null) => this.Name = new ItemName(originalName, englishName, shortName, otherNames);

        private void AddStatistic(int popularity, int dishCount, int ingredientCount) => this.Statistic = new RegionStatistic(popularity: popularity, dishCount: 0, ingredientCount: 0);
    }
}
