﻿namespace CookBook.Domain.DishAggregate
{
    using BuildingBlocks.Base;

    public interface IDishDomainRepository : IDomainRepository<Dish>
    {
    }
}
