﻿namespace CookBook.Domain.DishAggregate
{
    using BuildingBlocks.Base;
    using CookBook.Domain.Core;
    using System;

    public class Dish : Entity, IAggregateRoot
    {
        public string Description { get; private set; }

        public ItemName Name { get; private set; }
        public ItemStatistic Statistic { get; private set; }

        private Dish() { }

        public Dish(
            string originalName, 
            string englishName, 
            string shortName, 
            string otherNames, 
            long popularity, 
            string description)
        {

            this.Id = Guid.NewGuid();
            this.Description = description;
            this.AddNames(originalName, englishName, shortName, otherNames);
            this.AddStatistics(0, popularity, 0);
        }

        public void Update(
            string originalName, 
            string englishName, 
            string shortName, 
            string otherNames, 
            long popularity, 
            string description)
        {
            this.Description = description;
            this.AddNames(originalName, englishName, shortName, otherNames);
            this.AddStatistics(this.Statistic.CommentCount, popularity, this.Statistic.UsersRating);
        }

        private void AddNames(string originalName, string englishName, string shortName, string otherNames) => this.Name = new ItemName(originalName, englishName, shortName, otherNames);

        private void AddStatistics(long commentCount, long popularity, double rating) => this.Statistic = this.Statistic = new ItemStatistic(commentCount, popularity, rating);

    }
}
