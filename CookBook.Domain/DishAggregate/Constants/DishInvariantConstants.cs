﻿namespace CookBook.Domain.DishAggregate.Constants
{
    public class DishInvariantConstants
    {
        public static int GeneralMaxLengthText = 450;
        public static int GeneralMinLengthText = 2;
    }
}
