﻿namespace CookBook.Domain.DishAggregate.Exceptions
{
    using BuildingBlocks.Base;

    public class DishDomainException : DomainException
    {
        public DishDomainException(string message) : base(message)
        {
        }
    }
}
