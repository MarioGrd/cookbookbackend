﻿namespace CookBook.Domain.IngredientAggregate.Exceptions
{
    using BuildingBlocks.Base;

    public class IngridientDomainException : DomainException
    {
        public IngridientDomainException(string message) : base(message)
        {
        }
    }
}
