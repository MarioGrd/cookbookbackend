﻿namespace CookBook.Domain.IngredientAggregate
{
    using BuildingBlocks.Base;

    public interface IIngredientDomainRepository : IDomainRepository<Ingredient>
    {
    }
}
