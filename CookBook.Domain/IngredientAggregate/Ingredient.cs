﻿namespace CookBook.Domain.IngredientAggregate
{
    using BuildingBlocks.Base;
    using CookBook.Domain.Core;
    using CookBook.Domain.IngredientAggregate.Exceptions;
    using System;

    public class Ingredient : Entity, IAggregateRoot
    {
        public string Description { get; private set; }

        public ItemName Name { get; private set; }
        public ItemStatistic Statistic { get; private set; }

        public Guid RegionId { get; private set; }

        private Ingredient() { }

        public Ingredient(
            Guid regionId,
            string description,
            string originalName,
            string englishName,
            string shortName,
            string otherNames,
            long popularity)
        {
            if (regionId == null)
            {
                throw new IngridientDomainException("Region id is required.");
            }

            this.Id = Guid.NewGuid();
            this.RegionId = regionId;
            this.Description = description;
            this.AddNames(originalName, englishName, shortName, otherNames);
            this.AddStatistics(0, popularity, 0);
        }

        public void Update(
            Guid regionId,
            string originalName,
            string englishName,
            string shortName,
            string otherNames,
            long popularity,
            string description)
        {
            this.RegionId = regionId;
            this.Description = description;
            this.AddNames(originalName, englishName, shortName, otherNames);
            this.AddStatistics(this.Statistic.CommentCount, popularity, this.Statistic.UsersRating);
        }

        private void AddNames(string originalName, string englishName, string shortName, string otherNames) => this.Name = new ItemName(originalName, englishName, shortName, otherNames);

        private void AddStatistics(long commentCount, long popularity, double rating) => this.Statistic = this.Statistic = new ItemStatistic(commentCount, popularity, rating);
    }
}
