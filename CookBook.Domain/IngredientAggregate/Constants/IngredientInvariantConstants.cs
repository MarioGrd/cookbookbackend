﻿namespace CookBook.Domain.IngredientAggregate.Constants
{
    public class IngredientInvariantConstants
    {
        public static int GeneralMaxLengthText = 450;
        public static int GeneralMinLengthText = 2;
    }
}
