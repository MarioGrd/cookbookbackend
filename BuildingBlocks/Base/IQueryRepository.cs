﻿namespace BuildingBlocks.Base
{
    using Microsoft.EntityFrameworkCore;
    using System.Linq;

    public interface IQueryRepository<TEntity, TContext> where TEntity : Entity where TContext : DbContext
    {
        /// <summary>
        /// Returns <see cref="IQueryable{TEntity}"/> with no tracking.
        /// </summary>
        IQueryable<TEntity> Query { get; }
    }
}
