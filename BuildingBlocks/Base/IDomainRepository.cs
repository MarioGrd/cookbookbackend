﻿using System;
using System.Threading.Tasks;

namespace BuildingBlocks.Base
{
    public interface IDomainRepository<T> where T : IAggregateRoot
    {
        IUnitOfWork UnitOfWork { get; }

        void Add(T aggregateRoot);
        void Update(T aggregateRoot);
        void Delete(T aggregateRoot);
        Task<T> GetByIdAsync(Guid id);

    }
}
