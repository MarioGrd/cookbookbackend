﻿namespace BuildingBlocks.Base
{
    using System;

    public abstract class DomainException : Exception
    {
        public DomainException(string message) : base(message)
        {
        }    
    }
}
