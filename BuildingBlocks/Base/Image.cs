﻿namespace BuildingBlocks.Base
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public abstract class Image : Entity
    {
        public int ImageTypeId { get; private set; }
        public string ImagePath { get; private set; }
        public string Author { get; private set; }
        public string Source { get; private set; }

        protected Image() { }

        public Image(ImageType type, string path, string author = null, string source = null)
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                throw new ImageException("Path is required.");
            }

            if (!ImageType.List().Any(i => i.Id != type.Id))
            {
                throw new ImageException("Image type not suported");
            }

            this.Id = Guid.NewGuid();
            this.ImageTypeId = type.Id;
            this.ImagePath = path;
            this.Author = author;
            this.Source = source;
        }

    }

    public class ImageType : Enumeration
    {
        public static ImageType MAIN = new ImageType(1, nameof(MAIN).ToLowerInvariant());
        public static ImageType OTHER = new ImageType(2, nameof(OTHER).ToLowerInvariant());

        protected ImageType () { }

        protected ImageType(int id, string name) : base(id, name) { }

        public static IEnumerable<ImageType> List() => new[] { MAIN, OTHER };
    }

    public class ImageException : DomainException
    {
        public ImageException(string message) : base(message)
        {
        }
    }
}
