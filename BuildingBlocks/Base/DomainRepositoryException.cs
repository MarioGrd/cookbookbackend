﻿namespace BuildingBlocks.Base
{
    using System;

    public class DomainRepositoryException : Exception
    {
        public string RepositoryName { get; private set; }

        public DomainRepositoryException(string message, string repository) : base(message)
        {
            this.RepositoryName = repository;
        }
    }
}
